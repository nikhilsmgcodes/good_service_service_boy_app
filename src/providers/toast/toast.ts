import { Injectable } from '@angular/core';
import { ToastController } from  'ionic-angular';

/*
  Generated class for the ToastProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

@Injectable()
export class ToastProvider {

  constructor(private toastCtrl: ToastController) {
    //console.log('Hello ToastProvider Provider');
  }

  notify(showdata, css){
    let toast = this.toastCtrl.create({
      message: showdata, 
      duration: 3000, 
      position: 'top',
      cssClass : css
      // dismissOnPageChange: true
    }); 
    toast.present();
  }

  
  

}
