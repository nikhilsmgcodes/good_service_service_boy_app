import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { ProfilePage } from '../pages/profile/profile';
//import { LeaddetailsPage } from '../pages/leaddetails/leaddetails';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  pages: Array<{title: string, iconimg: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public alertCtrl:AlertController) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Leads', iconimg:'eye', component: HomePage },
      { title: 'Profile', iconimg:'person', component: ProfilePage },
      { title: 'Logout', iconimg:'exit', component: null}
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if(page.title=='Logout'){
      let alert=this.alertCtrl.create({
        message:'Are you sure you want to log out?',
        buttons:[{
             text:'Cancel',
             handler:()=>{
               console.log('cancel clicked');
             }
        },
          {
              text:'Ok',
              handler:()=>{
                localStorage.clear();
                this.nav.setRoot(LoginPage);
              }
        }
        ]
      });
        alert.present();
    }
    else{
      this.nav.setRoot(page.component);
    }
  }
}
