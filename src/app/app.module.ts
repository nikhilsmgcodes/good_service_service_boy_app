import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { Camera } from '@ionic-native/camera';
import { Media } from '@ionic-native/media';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
//import { MediaCapture} from '@ionic-native/media-capture';
import { Base64 } from '@ionic-native/base64';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { LeaddetailsPage } from '../pages/leaddetails/leaddetails';
import { AddfollowupinfoPage } from '../pages/addfollowupinfo/addfollowupinfo';
import { ProfilePage } from '../pages/profile/profile';
// import { BilldetailsPage } from '../pages/billdetails/billdetails';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ToastProvider } from '../providers/toast/toast';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    LeaddetailsPage,
    AddfollowupinfoPage,
    ProfilePage,
    // BilldetailsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    LeaddetailsPage,
    AddfollowupinfoPage,
    ProfilePage,
    // BilldetailsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    ToastProvider,
    Media, 
    File,
    FileOpener,
    //MediaCapture,
    Base64,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
