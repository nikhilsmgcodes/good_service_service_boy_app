import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddfollowupinfoPage } from './addfollowupinfo';

@NgModule({
  declarations: [
    AddfollowupinfoPage,
  ],
  imports: [
    IonicPageModule.forChild(AddfollowupinfoPage),
  ],
})
export class AddfollowupinfoPageModule {}
