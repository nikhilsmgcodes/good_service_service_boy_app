import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, AlertController, Platform } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Rx';

import { ToastProvider } from '../../providers/toast/toast';
import { Media, MediaObject } from '@ionic-native/media';
import { Base64 } from '@ionic-native/base64';
import { File } from '@ionic-native/file';

import { HomePage } from '../home/home';
import { LoginPage } from '../login/login';

/**
 * Generated class for the AddfollowupinfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addfollowupinfo',
  templateUrl: 'addfollowupinfo.html',
})
export class AddfollowupinfoPage {
  httpOptions : any;
  url : string = 'softbizz.in/goodservice/api/public/';
  data : string;
  status: any;
  leadsDetails: any;
  description: any;

  public photos : any = [];
  public base64Image : string;
  private imageSrc : string = '';
  recording: boolean = false;
  fileName:any;
  filePath:any;
  audio: MediaObject;
  followUpDate:any;
  selectedAudiofile:any;
  filePathDir:any;

  billDescription: any = null;
  billAmount:any = null;
  gst:any;
  totalAmount:any = this.billAmount;
  minDate: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
              public http: Http, private camera: Camera, 
              private toast: ToastProvider, public actionSheetCtrl: ActionSheetController, 
              public alertCtrl: AlertController, private media: Media,
              public platform: Platform, private file: File,
              private base64: Base64
            ) {
    this.status = navParams.get('status');
    this.leadsDetails = navParams.get('leadsDetails');
  }

  ionViewDidLoad() {
    console.log(this.status);
    console.log(this.leadsDetails);
    this.followUpDate = this.formatDate(new Date());
    this.minDate = this.formatDate(new Date());
  }

  formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }

  passAmount(){
    console.log('passAmount');
    if(this.gst){
      console.log('yes');
      this.totalAmount = this.billAmount *18/100 + parseInt(this.billAmount);
    }
    else{
      console.log('nos');
      this.totalAmount = this.billAmount;
    }
  }

  gstCalculate(){
    console.log('gst');
    if(this.billAmount != null){
      if(this.gst){
        console.log('yes');
        this.totalAmount = this.billAmount *18/100 + parseInt(this.billAmount);
      }
      else{
        console.log('nos');
        this.totalAmount = this.billAmount;
      }   
    }
  }
  
  public sendInfo(){
    let formData = new FormData();
          if(this.photos){
             for(var i=0;i<this.photos.length;i++){
             formData.append('images[]', this.photos[i]);
           }
          }
          
          formData.append('audioo', this.selectedAudiofile);
          
          if(this.followUpDate==null){
            return alert('followUp Date Not Choosen ');
          }
        formData.append('status',this.status);
        formData.append('msg',this.description);
        formData.append('followUpDate',this.followUpDate);
        formData.append('l_id', this.leadsDetails.l_id);
        formData.append('c_phone', this.leadsDetails.c_phone);
        formData.append('st_phone',localStorage.getItem('user_phone'));

        if(this.status == 'COMPLETED'){
          if(this.billDescription == null){
            this.toast.notify('Please Enter Bill Details','toast-error');
            return;
          }
          if(this.billAmount==null){
            this.toast.notify('Please Enter Bill Amount','toast-error');
            return;
          }
          formData.append('billDescription', this.billDescription);
          formData.append('billAmount', this.billAmount);
          formData.append('gst',this.gst);
        }

        
        let headers = new Headers({
            'Accept':  'application/json',
            // 'Content-Type':  'application/x-www-form-urlencoded',
            'user-phone': JSON.parse(localStorage.getItem('user_phone')),
            'session-key': JSON.parse(localStorage.getItem('session_key')),
            'role': localStorage.getItem('role')
          });
        
        let options = new RequestOptions({ headers: headers });

        this.http.post('http://'+this.url+'addFollowUpInfoApp', formData, options).map(res => res.json()).catch(error => Observable.throw(error)).subscribe(
                data => {
                  if(data.sessionExpire){
                      this.navCtrl.setRoot(LoginPage);
                      this.toast.notify(data.message,'toast-error');
                    }
                  else{
                    if(data.error){
                      this.toast.notify(data.message,'toast-error');
                      //this.st_details = null;
                    }
                    else{
                      this.toast.notify(data.message,'toast-error');
                      this.navCtrl.setRoot(HomePage);
                      // this.description=null;
                      // this.followUpDate=null;
                      // this.myInputVariableImage.nativeElement.value = "";
                      // this.myInputVariableAudio.nativeElement.value = "";
                      // //this.getFollowUpInfo(this.l_id);
                      // this.getAcceptedLeads();
                      // this.showDialogFollowUp = false;
                      // this.showDialogCompleted = false;

                    }
                  }

                },
                 error => this.toast.notify(error,'toast-error')
            )
  }

  public cancel(){
    this.navCtrl.pop();
  }

  public playAudio(){
    if (this.platform.is('ios')) {
      this.filePath = this.file.documentsDirectory.replace(/file:\/\//g, '') + this.fileName;
      this.audio = this.media.create(this.filePath);
    } else if (this.platform.is('android')) {
      this.filePath = this.file.externalDataDirectory.replace(/file:\/\//g, '') + this.fileName;
      this.audio = this.media.create(this.filePath);
    }
    this.audio.play();
    //this.audio.setVolume(0.8);
  }

  public startRecord(){
    if (this.platform.is('ios')) {
      this.fileName = 'audiofile'+'.mp3';
      this.filePathDir = this.file.documentsDirectory.replace(/file:\/\//g, '');
      this.filePath = this.file.documentsDirectory.replace(/file:\/\//g, '') + this.fileName;
      this.audio = this.media.create(this.filePath);
    } else if (this.platform.is('android')) {
      this.fileName = 'audiofile'+'.mp3';
      this.filePathDir = this.file.externalDataDirectory.replace(/file:\/\//g, '');
      this.filePath = this.file.externalDataDirectory.replace(/file:\/\//g, '') + this.fileName;
      this.audio = this.media.create(this.filePath);
    }
    
    this.audio.startRecord();
    this.recording = true;
    
  }

  public stopRecord(){
    this.audio.stopRecord();
    this.recording = false;
    this.convertAudioToBase64();
  }

  convertAudioToBase64(){
    let filePath: string = this.filePath;
    this.base64.encodeFile(filePath).then((base64File: string) => {
      this.selectedAudiofile = base64File;
    }, (err) => {
      this.toast.notify(err, 'toast-error');
    });
  }

  public deletePhoto(index) {
    let confirm = this.alertCtrl.create({
        title: 'Sure you want to delete this photo?',
        message: '',
        buttons: [
          {
            text: 'No',
            handler: () => {
              //console.log('Disagree clicked');
            }
          }, {
            text: 'Yes',
            handler: () => {
              //console.log('Agree clicked');
              this.photos.splice(index, 1);
            }
          }
        ]
      });
    confirm.present();
  }

  public openGallery () {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Use Gallery',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            let cameraOptions = {
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            destinationType: this.camera.DestinationType.DATA_URL,
            quality: 100,
            allowEdit : true,
            targetWidth: 300,
            targetHeight: 300,
            encodingType: this.camera.EncodingType.JPEG,
            correctOrientation: true
          }

          this.camera.getPicture(cameraOptions)
            .then(file_uri => {
              
              this.imageSrc = 'data:image/jpeg;base64,' +file_uri;
      
              this.photos.push(this.imageSrc);
              this.photos.reverse();
            },
            err => this.toast.notify(err, 'toast-error'));
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            let cameraOptions = {
            sourceType: this.camera.PictureSourceType.CAMERA,
            destinationType: this.camera.DestinationType.DATA_URL,
            quality: 100,
            targetWidth: 300,
            targetHeight: 300,
            encodingType: this.camera.EncodingType.JPEG,
            correctOrientation: true,
            allowEdit : true
          }

          this.camera.getPicture(cameraOptions)
            .then(file_uri => {
              
              this.imageSrc = 'data:image/jpeg;base64,' +file_uri;
              
              this.photos.push(this.imageSrc);
              this.photos.reverse();
            },
            err => this.toast.notify(err, 'toast-error'));
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }
}
