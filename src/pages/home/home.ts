import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

import { LoginPage } from '../login/login';
import { LeaddetailsPage } from '../leaddetails/leaddetails';

import { ToastProvider } from '../../providers/toast/toast';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  httpOptions : any;
  url : string = 'softbizz.in/goodservice/api/public/';
  data : string;
  allLeads: any;
  status: any = 'ASSIGNED';

  constructor(public navCtrl: NavController, public http: Http, public toast: ToastProvider) {

  }

  ionViewDidEnter() {
    console.log('ionViewDidLoad LoginPage');
    this.getLeads(this.status);
  }

  public itemSelected(x){
    this.navCtrl.push(LeaddetailsPage,{data:x});
  }

  public getLeads(status){
    this.data = 'status='+status;
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.post('http://'+this.url+'getLeadsForServiceTeam',this.data, this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.navCtrl.setRoot(LoginPage);
          //this.navCtrl.setRoot(LoginPage, {'data':data});
          //this.navParams.get('data');
          this.toast.notify(data.message,'toast-error');
        }
      else{
        if(data.error){
          this.allLeads=data.data;
          this.toast.notify(data.message,'toast-error');
        }
        else{
          this.allLeads=data.data;
          //alert(data.message);
          //console.log(this.allLeadsDetail);
        }
      }
    });
  }
}
