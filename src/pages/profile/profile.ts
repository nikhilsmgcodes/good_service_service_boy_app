import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

import { LoginPage } from '../login/login';

import { ToastProvider } from '../../providers/toast/toast';
/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  httpOptions : any;
  url : string = 'softbizz.in/goodservice/api/public/';
  data : string;

  st_details:any={st_phone:null, name:null, address:null, city:null, state:null, zip:null, aadhaar:null, photo:null};
  st_category:any;
  category = ['AC','TV','WASHING-MACHINE','REFRIGERATOR','CHIMNEY','WATER-PURIFIER','MICROWAVE-OVEN','GEYSER'];
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public toast: ToastProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
    this.getProfileInfo();

  }

  public getProfileInfo(){
    this.data = 'st_phone='+JSON.parse(localStorage.getItem('user_phone'));
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };

    this.http.post('http://'+this.url+'viewServiceTeamDetailsByPhone',this.data, this.httpOptions).map(res => res.json()).subscribe(
      data => {
        if(data.sessionExpire){
            this.navCtrl.setRoot(LoginPage);
            this.toast.notify(data.message,'toast-error');
          }
        else{
          if(data.error){
            this.toast.notify(data.message,'toast-error');
          }
          else{
            this.st_details=data.st_details[0];
            this.st_category=data.st_category[0];
          }
        }
      });
    }
}
