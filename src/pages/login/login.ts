import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

import { HomePage } from '../home/home'

import { ToastProvider } from '../../providers/toast/toast';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  httpOptions : any;
  url : string = 'softbizz.in/goodservice/api/public/';
  data : string;
  userPhone : any = null;
  userPass : any = null;
  tabBarElement:any;
  constructor(public navCtrl: NavController, private menu: MenuController, public navParams: NavParams, public http: Http, public toast: ToastProvider) {
  }

  ionViewWillEnter() {
    console.log('ionViewDidLoad LoginPage');
    this.menu.swipeEnable(false);
    if (this.tabBarElement != null) {
      this.tabBarElement.style.display = 'none'; // or whichever property which you want to access
    }
    this.checkSession();
  }

  ionViewWillLeave() {
    if (this.tabBarElement != null) {
      this.tabBarElement.style.display = 'flex';
    }
    this.menu.swipeEnable(true);
   }

  public checkSession(){
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.get('http://'+this.url+'checkSessionAdmin',this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.error){
        // this.navCtrl.push(LoginPage);
      }
      else{
        this.navCtrl.setRoot(HomePage);
        //window.location.reload();
      }
    })
  }

  public doLogin(){
    if(this.userPhone == null || this.userPhone.length != 10){
      this.toast.notify("Please Enter 10 digit Phone Number",'toast-error');
      return;
    }
    if(this.userPass == null){
      this.toast.notify("Please Enter Password",'toast-error');
      return;
    }
    else{
      this.data = "user_phone="+this.userPhone+'&user_pass='+this.userPass+'&role='+"serviceTeam";

      this.httpOptions = {
          headers: new Headers({
            'Content-Type':  'application/x-www-form-urlencoded'
          })
        };
      this.http.post('http://'+this.url+'adminLogin', this.data, this.httpOptions).map(res => res.json()).subscribe(
        data => {
          if(data.error){
            this.toast.notify(data.message,'toast-error');
          }else{
            localStorage.setItem('session_key', JSON.stringify(data.session_key));
            localStorage.setItem('user_phone', this.userPhone);
            localStorage.setItem('role', "serviceTeam");
            //localStorage.setItem('tableName', 'admin');
            this.navCtrl.setRoot(HomePage);
          }
        });
      }
  }

}
