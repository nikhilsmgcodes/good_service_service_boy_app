import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController } from 'ionic-angular';

import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

import { ToastProvider } from '../../providers/toast/toast';

import { HomePage } from '../home/home'
import { AddfollowupinfoPage } from '../addfollowupinfo/addfollowupinfo';
import { LoginPage } from '../login/login';
// import { BilldetailsPage } from '../billdetails/billdetails';

/**
 * Generated class for the LeaddetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-leaddetails',
  templateUrl: 'leaddetails.html',
})
export class LeaddetailsPage {
  httpOptions : any;
  url : string = 'softbizz.in/goodservice/api/public/';
  data : string;
  leadinfo: any;
  lead: any = 'details';
  leadsDetails : any ={category:null, brand:null,description:null,date:null,status:null};
  customerAddress : any = {c_phone:null, name:null, address:null, city:null, state:null, zip:null, optional_phone:null};
  followupmsg : any;
  bills:any;
  withoutGstBills:any;
  withGstBills:any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,public alertCtrl: AlertController, 
              public http: Http, public toast: ToastProvider, public modalCtrl : ModalController) {
    this.leadinfo = navParams.get('data');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LeaddetailsPage');
    this.getLeadInfo(this.leadinfo.l_id);
    this.getFollowUpInfo(this.leadinfo.l_id);
    this.allBills();
  }

  public viewBill(b){
    var modalPage = this.modalCtrl.create('BilldetailsPage', { bill: b});
    console.log(b);
    modalPage.present();
  }

  public addFollowUpInfo(status){
    this.navCtrl.push(AddfollowupinfoPage,{status:status, leadsDetails:this.leadsDetails});
  }

  public reject(l_id){
    let alert=this.alertCtrl.create({
      message:'Reject This Lead?',
      buttons:[{
           text:'Cancel',
           handler:()=>{
             console.log('cancel clicked');
           }
      },
        {
            text:'Ok',
            handler:()=>{
              this.data = 'l_id='+l_id;
              this.httpOptions = {
                headers: new Headers({
                  'Content-Type':  'application/x-www-form-urlencoded',
                  'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                  'session-key': JSON.parse(localStorage.getItem('session_key')),
                  'role': localStorage.getItem('role')
                })
              };
              this.http.post('http://'+this.url+'unAssignLeadAdmin',this.data, this.httpOptions).map(res => res.json()).subscribe(
              data => {
                if(data.sessionExpire){
                    this.navCtrl.setRoot(LoginPage);
                    this.toast.notify(data.message,'toast-error');
                  }
                else{
                  if(data.error){
                    this.toast.notify(data.message,'toast-error');
                  }
                  else{
                    this.navCtrl.setRoot(HomePage);
                  }
                }
              });
            }
      }
      ]
    });
      alert.present();
  }

  public accepted(l_id){
    let alert=this.alertCtrl.create({
      message:'Accept This Lead?',
      buttons:[{
           text:'Cancel',
           handler:()=>{
             console.log('cancel clicked');
           }
      },
        {
            text:'Ok',
            handler:()=>{
              var st_phone = localStorage.getItem('user_phone');
              this.data = 'l_id='+l_id+'&st_phone='+st_phone;
              this.httpOptions = {
                headers: new Headers({
                  'Content-Type':  'application/x-www-form-urlencoded',
                  'user-phone': JSON.parse(localStorage.getItem('user_phone')),
                  'session-key': JSON.parse(localStorage.getItem('session_key')),
                  'role': localStorage.getItem('role')
                })
              };
              this.http.post('http://'+this.url+'leadAcceptedAdmin',this.data, this.httpOptions).map(res => res.json()).subscribe(
              data => {
                if(data.sessionExpire){
                    this.navCtrl.setRoot(LoginPage);
                    this.toast.notify(data.message,'toast-error');
                  }
                else{
                  if(data.error){
                    this.toast.notify(data.message,'toast-error');
                  }
                  else{
                    this.getLeadInfo(this.leadinfo.l_id);
                  }
                }
              });
            }
      }
      ]
    });
      alert.present();
  }

  public getLeadInfo(l_id){
    this.data = 'l_id='+l_id;
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.post('http://'+this.url+'getLeadDetailByIdAdmin',this.data, this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.navCtrl.setRoot(LoginPage);
          this.toast.notify(data.message,'toast-error');
        }
      else{
        if(data.error){
          this.toast.notify(data.message,'toast-error');
        }
        else{
          this.leadsDetails = data.lead_data[0];
          this.customerAddress = data.customer_address[0];
        }
      }
    });
  }

  public getFollowUpInfo(l_id){
    this.data = 'l_id='+l_id;
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.post('http://'+this.url+'getFollowUpInfoAdmin',this.data, this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.navCtrl.setRoot(LoginPage);
          this.toast.notify(data.message,'toast-error');
        }
      else{
        if(data.error){
          this.toast.notify(data.message,'toast-error');
        }
        else{
          this.followupmsg=data.msg;
        }
      }
    });
  }

  public allBills(){
    this.data = 'l_id='+this.leadinfo.l_id;
    this.httpOptions = {
      headers: new Headers({
        'Content-Type':  'application/x-www-form-urlencoded',
        'user-phone': JSON.parse(localStorage.getItem('user_phone')),
        'session-key': JSON.parse(localStorage.getItem('session_key')),
        'role': localStorage.getItem('role')
      })
    };
    this.http.post('http://'+this.url+'bills',this.data, this.httpOptions).map(res => res.json()).subscribe(
    data => {
      if(data.sessionExpire){
          this.navCtrl.setRoot(LoginPage);
          this.toast.notify(data.message,'toast-error');
        }
      else{
        if(data.error){
          this.toast.notify(data.message,'toast-error');
        }
        else{
          this.withoutGstBills=data.withoutGstBills;
          this.withGstBills=data.withGstBills;
        }
      }
    });
  }

}
